const commander = require('commander'),
      { version } = require('./package.json'),
      console = require('console')

// commands
// const create = require('./commands/create');
// const setup = require('./commands/setup');

commander
  .version(version);

// commander
//   .command('create')
//   .description('Create a new project')
//   .action(setup);

commander
  .command('*')
  .action(() => {
    commander.help()
  })

commander.parse(process.argv);

if (!commander.args.length) {
  commander.help()
}